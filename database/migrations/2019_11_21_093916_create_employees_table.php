<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ebranch_name');
            $table->string('eid');
            $table->string('ename');
            $table->string('efname');
            $table->string('edesignation');
            $table->string('eemail');
            $table->string('emobile');
            $table->string('edate_of_birth');
            $table->string('enational_id');
            $table->string('epresent_add');
            $table->string('eparmanent_add');
            $table->string('edescription');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
