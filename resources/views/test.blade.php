@extends('layouts.app')

@section('content')
<div class="text-right mb-3">
    <a href="{{url('/new/company/list')}}" class="btn  btn-sm active com text-white">Company List</a>
</div>
  <div class=" btn btn-block btn-sm active mb-3 com text-white" >New Company</div>

  <form method=""  action="" enctype="multipart/form-data">
    @csrf
      <div class="form-group row col-md-8 offset-md-2">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Group Name*</label>
        <div class="col-sm-10 mt-3">
          <select class="form-control" name="group_id">
            <option>-- select one --</option>
          </select>
        </div>
            <label for="inputEmail3" class="col-sm-2 col-form-label">Company Name*</label>
              <div class="col-sm-10 mt-2" >
                <input type="text"  name="cname" class="form-control"  placeholder="Enter Company name">
              </div>
                <label for="inputEmail3" class="col-sm-2 col-form-label">Mobile*</label>
                    <div class="col-sm-10 mt-2" >
                      <input type="number"  name="cphone" class="form-control"  placeholder="Enter company Phone">
                </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10 mt-2" >
                          <input type="text"  name="cemail" class="form-control"  placeholder="Enter company Email ">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10 mt-2" >
                      <textarea type="text"  name="caddress" class="form-control" ></textarea>
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Website</label>
                    <div class="col-sm-10 mt-2">
                      <input type="text"  name="cweb" class="form-control"  placeholder="Enter website">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label"> Upload Image</label>
                      <div class="col-sm-10 mt-3" >
                        <input type="file" name="image_name" class="form-control-file" id="exampleFormControlFile1">
                      </div>

                        <label for="inputEmail3" class="col-sm-2 col-form-label mt-5"></label>

                          <div class="col-sm-6 mt-5">
                         <button class="btn  btn-sm active com text-white" >submit</button>
                           <a href="#" class="btn btn-danger btn-sm active">Close</a>
                         </div >
                         <div class="col-sm-4 text-right mt-5">
                        <a class="  btn  btn-sm active com text-white" href="{{url('new')}}" >Next</a>
                      </div>

                    </div>


                      </form>

                 @endsection


<!--
     <div class="pt-4">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header ">New Product</div>

                        <div class="card-body">
                            <form method="" action="">
                                @csrf
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Login') }}
                                        </button>
                                        <button type="submit" class="btn btn-danger">
                                            {{ __('close') }}
                                        </button>
                                        <button type="submit" class="text-right offset-4 btn btn-success">
                                            {{ __('Next') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>  -->
