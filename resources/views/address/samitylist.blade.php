
@extends('layouts.app')

@section('content')
    <div class="text-right mb-3">
        <a href="{{url('samity')}}" class="btn btn-sm active com text-white">Add</a>
    </div>
<div >
    <label class="row justify-content-center"><h4><strong>Samity List</strong></h4></label>
  </div>
    <table class="table table-bordered table-sm text-center ">
      <thead class="text-white">
        <tr  class="com">
          <th class="">SL No.</th>
          <th scope="col">Branch Name</th>
          <th scope="col">Samity Name</th>
          <th scope="col">Action</th>
          </tr>
      </thead>
      <tbody>
        @foreach($samities as $samity)
        <tr>
          <td>{{$samity->id}}</td>
          <td scope="row">{{$samity->sbranch_name}}</td>
          <td>{{$samity->samity}}</td>
          <td><a href="{{url('samityedit'.$samity->id)}}" class="btn btn-warning">Update</a>
            <a href="{{url('delete/samity'.$samity->id)}}" class="btn btn-warning">DELETE</a></td> 
        </tr>
        @endforeach
      </tbody>
    </table>
@endsection
