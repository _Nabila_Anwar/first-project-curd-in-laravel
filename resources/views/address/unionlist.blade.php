@extends('layouts.app')

@section('content')
    <div class="text-right mb-3">
        <a href="{{url('union_zone')}}" class="btn btn-sm active com text-white">Add</a>
    </div>
<div >
    <label class="row justify-content-center"><h4><strong>Union List</strong></h4></label>
  </div>
    <table class="table table-bordered table-sm text-center ">
      <thead class="text-white">
        <tr  class="com">
          <th class="">SL No.</th>
          <th scope="col">Division</th>
          <th scope="col">District</th>
          <th scope="col">Upzilla</th>
          <th scope="col">Union</th>
          <th scope="col">Action</th>
          </tr>
      </thead>
      <tbody>
        @foreach($unions as $union)
        <tr>
          <td>{{$union->id}}</td>
          <td scope="row">{{$union->udivision}}</td>
          <td>{{$union->udistrict}}</td>
          <td class="text-center"><a href=""><i class="fa fa-trash" aria-hidden="true"></i></a>
          <a href=""> <i class="fa fa-edit"></i></a>{{$union->uupzilla}}</td>
          <td>{{$union->uunion}}</td>
          <td><a href="{{url('unionedit'.$union->id)}}" class="btn btn-warning">EDIT</a></td>      
        </tr> 
        @endforeach     
      </tbody>
    </table>
@endsection
