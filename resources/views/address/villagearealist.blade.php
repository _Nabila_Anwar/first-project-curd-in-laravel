@extends('layouts.app')

@section('content')
    <div class="text-right mb-3">
        <a href="{{url('village_area')}}" class="btn btn-sm active com text-white">Add</a>
    </div>
<div >
    <label class="row justify-content-center"><h4><strong>Village/Area List</strong></h4></label>
  </div>
    <table class="table table-bordered table-sm text-center ">
      <thead class="text-white">
        <tr  class="com">
          <th class="">SL No.</th>
          <th scope="col">Division</th>
          <th scope="col">District</th>
          <th scope="col">Upzilla</th>
          <th scope="col">Union</th>
          <th scope="col">Village</th>
          </tr>
      </thead>
      <tbody>
        @foreach($villages as $village)
        <tr>
          <td>{{$village->id}}</td>
          <td scope="row">{{$village->vdivision}}</td>
          <td>{{$village->vdistrict}}</td>
          <td class="text-center"><a href=""><i class="fa fa-trash" aria-hidden="true"></i></a>
          <a href=""> <i class="fa fa-edit"></i></a>{{$village->vupozilla}}</td>
          <td>{{$village->vunion}}</td>
          <td>{{$village->vvillage}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
@endsection
