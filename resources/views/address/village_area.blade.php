@extends('layouts.app')

@section('content')
<div class="text-right mb-3">
    <a href="{{url('villagelist')}}" class="btn  btn-sm active com text-white">Village List</a>
</div>
<div class="card">
  <div class="card-heading text-center p-1 com text-white" >New Village</div>
  <form method="post"  action="{{url('/village_area')}}" enctype="multipart/form-data">
    @csrf
      <div class="card-body form-group row col-md-8 offset-md-2">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Division*</label>
        <div class="col-sm-10 mt-3">
          <!--<select class="form-control" name="vdivision_id">
            <option>-- Select Devision --</option>
          </select>-->
          <input type="text"  name="vdivision" class="form-control"  placeholder="Enter Division">
        </div>
            <label for="inputEmail3" class="col-sm-2 col-form-label">District*</label>
              <div class="col-sm-10 mt-2" >
                <input type="text"  name="vdistrict" class="form-control"  placeholder="Enter District">
              </div>
                <label for="inputEmail3" class="col-sm-2 col-form-label">Upozilla*</label>
                    <div class="col-sm-10 mt-2" >
                      <input type="text"  name="vupozilla" class="form-control"  placeholder="Enter Upozilla">
                </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Union/Zone Name*</label>
                        <div class="col-sm-10 mt-2" >
                          <input type="text"  name="vunion" class="form-control"  placeholder="Enter Union/Zone">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Village/Locality Name*</label>
                    <div class="col-sm-10 mt-2">
                      <input type="text" name="vvillage" class="form-control" placeholder="Enter Village/Locality">
                    </div>
                        <label for="inputEmail3" class="col-sm-2 col-form-label mt-5"></label>

                          <div class="col-sm-6 mt-5">
                         <button class="btn  btn-sm active com text-white" type="submit">submit</button>
                           <a href="#" class="btn btn-danger btn-sm active">Close</a>
                         </div >
                    </div>


                      </form>

</div>
                 @endsection



