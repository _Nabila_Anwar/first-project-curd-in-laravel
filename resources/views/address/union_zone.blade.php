@extends('layouts.app')

@section('content')

<div class="text-right mb-3">
    <a href="{{url('unionlist')}}" class="btn  btn-sm active com text-white">Union/Zone List</a>
</div>
<div class="card">
  <div class="card-heading text-center p-1 com text-white" >New Union/Zone</div>
  <form method="post"  action="{{url('/union_zone')}}" enctype="multipart/form-data">
    @csrf
      <div class="card-body form-group row col-md-8 offset-md-2">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Division*</label>
        <div class="col-sm-10 mt-3">
          <input type="text"  name="udivision" class="form-control"  placeholder="Enter Division">
        </div>
            <label for="inputEmail3" class="col-sm-2 col-form-label">District*</label>
              <div class="col-sm-10 mt-2" >
                <input type="text"  name="udistrict" class="form-control"  placeholder="Enter District">
              </div>
                <label for="inputEmail3" class="col-sm-2 col-form-label">Upozilla*</label>
                    <div class="col-sm-10 mt-2" >
                      <input type="text"  name="uupzilla" class="form-control"  placeholder="Enter Upozilla">
                </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Union/Zone Name*</label>
                        <div class="col-sm-10 mt-2" >
                          <input type="text"  name="uunion" class="form-control"  placeholder="Enter Union/Zone">
                        </div>
                        <label for="inputEmail3" class="col-sm-2 col-form-label mt-5"></label>

                        <div class="col-sm-6 mt-5">
                        <button class="btn  btn-sm active com text-white" type="submit" >submit</button>
                           <a href="#" class="btn btn-danger btn-sm active">Close</a>
                        </div >
                    </div>
                      </form>
</div>

                 @endsection



