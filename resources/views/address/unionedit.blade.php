@extends('layouts.app')

@section('content')

<div class="text-right mb-3">
    <a href="{{url('unionlist')}}" class="btn  btn-sm active com text-white">Union/Zone List</a>
</div>
<div class="card">
  <div class="card-heading text-center p-1 com text-white" >New Union/Zone</div>
  <form method="post"  action="{{url('/unionedit'.$union->id)}}" enctype="multipart/form-data">
    @csrf
      <div class="card-body form-group row col-md-8 offset-md-2">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Division*</label>
        <div class="col-sm-10 mt-3">
          <input type="text"  name="udivision" class="form-control"  value="$union->udivision">
        </div>
            <label for="inputEmail3" class="col-sm-2 col-form-label">District*</label>
              <div class="col-sm-10 mt-2" >
                <input type="text"  name="udistrict" class="form-control"  value="$union->udistrict">
              </div>
                <label for="inputEmail3" class="col-sm-2 col-form-label">Upozilla*</label>
                    <div class="col-sm-10 mt-2" >
                      <input type="text"  name="uupzilla" class="form-control"  value="$union->uupzilla">
                </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Union/Zone Name*</label>
                        <div class="col-sm-10 mt-2" >
                          <input type="text"  name="uunion" class="form-control"  value="$union->uunion">
                        </div>
                        <label for="inputEmail3" class="col-sm-2 col-form-label mt-5"></label>

                        <div class="col-sm-6 mt-5">
                        <button class="btn  btn-sm active com text-white" type="submit" >Update</button>
                           <a href="#" class="btn btn-danger btn-sm active">Close</a>
                        </div >
                    </div>
                      </form>
</div>

                 @endsection



