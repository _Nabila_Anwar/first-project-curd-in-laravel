@extends('layouts.app')

@section('content')
    <div class="text-right mb-3">
        <a href="{{url('image')}}" class="btn btn-sm active com text-white">Add</a>
    </div>
<div >
    <label class="row justify-content-center"><h4><strong>Image Table</strong></h4></label>
  </div>
    <table class="table table-bordered table-sm text-center ">
      <thead class="text-white">
        <tr  class="com">
          <th class="">ID</th>
          <th scope="col">First Name</th>
          <th scope="col">Email</th>
          <th scope="col">Post</th>
          <th scope="col">Image</th>
          </tr>
      </thead>
      <tbody>
        @foreach($images as $image) 
        <tr>
          <td>{{$image->id}}</td>
          <td scope="row">{{$image->fname}}</td>
          <td>{{$image->email}}</td>
          <td class="text-center"><a href=""><i class="fa fa-trash" aria-hidden="true"></i></a>
          <a href=""><i class="fa fa-edit"></i></a>{{$image->post}}</td>
          <td><img src="{{asset('uploads/imagefolder/'. $image->image)}}" width="100px;" height="100px;" alt="Image"></td>
        </tr>
        @endforeach
      </tbody>
    </table>
@endsection
