@extends('layouts.app')

@section('content')
    <div class="text-right mb-3">
        <a href="{{url('/new/product/group')}}" class="btn btn-sm active com text-white">Add</a>
    </div>
<div >
    <label class="row justify-content-center"><h4><strong>Product Group</strong></h4></label>
  </div>
    <table class="table table-bordered table-sm text-center ">
      <thead class="text-white">
        <tr  class="com">
          <th class="">SL No</th>
          <th scope="col">Product group</th>
          <th scope="col">Code</th>
          <th scope="col">Action</th>
          </tr>
      </thead>
      <tbody>

        <tr>
          <td>eer</td>
          <td scope="row">sdf</td>
            <td></td>
          <td class="text-center"><a href=""><i class="fa fa-trash" aria-hidden="true"></i></a>
            <a href="">	<i class="fa fa-edit"></i></a>
           </td>
        </tr>
      </tbody>
    </table>
@endsection
