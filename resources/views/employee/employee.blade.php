@extends('layouts.app')

@section('content')
<div class="text-right mb-3">
    <a href="{{url('employee_list')}}" class="btn  btn-sm active com text-white">Employee List</a>
</div>
  <div class=" btn btn-block btn-sm active mb-3 com text-white" >New Employee</div>

  <form method="post"  action="{{url('/employee')}}" enctype="multipart/form-data">
    @csrf
      <div class="form-group row col-md-8 offset-md-2">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Branch Name</label>
        <div class="col-sm-10 mt-3">
          <!--<select class="form-control" name="ebranch_id">
            <option>-- Head Office --</option>
          </select>-->
          <input type="text"  name="ebranch_name" class="form-control"  placeholder="Enter Branch Name">
        </div>
            <label for="inputEmail3" class="col-sm-2 col-form-label">Employee ID</label>
              <div class="col-sm-10 mt-2" >
                <input type="text"  name="eid" class="form-control"  placeholder="Enter Employee ID">
              </div>
                <label for="inputEmail3" class="col-sm-2 col-form-label">Employee Name*</label>
                    <div class="col-sm-10 mt-2" >
                      <input type="text"  name="ename" class="form-control"  placeholder="Enter Employee Name">
                </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Father's Name</label>
                        <div class="col-sm-10 mt-2" >
                          <input type="text"  name="efname" class="form-control"  placeholder="Enter Father's Name">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Designation</label>
                        <div class="col-sm-10 mt-2" >
                          <input type="text"  name="edesignation" class="form-control"  placeholder="Enter Designation">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10 mt-2" >
                          <input type="text"  name="eemail" class="form-control"  placeholder="Enter Email">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Mobile*</label>
                        <div class="col-sm-10 mt-2" >
                          <input type="text"  name="emobile" class="form-control"  placeholder="Enter Phone">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Date Of Birth</label>
                        <div class="col-sm-10 mt-2" >
                          <input type="date"  name="edate_of_birth" class="form-control input-md hasDatepicker"  placeholder=" ">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">National ID*</label>
                        <div class="col-sm-10 mt-2" >
                          <input type="text"  name="enational_id" class="form-control"  placeholder="Enter National ID No">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Present Address</label>
                        <div class="col-sm-10 mt-2" >
                          <textarea type="text"  name="epresent_add" class="form-control"  placeholder=" "></textarea> 
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Parmanent Address</label>
                        <div class="col-sm-10 mt-2" >
                          <textarea type="text"  name="eparmanent_add" class="form-control"  placeholder=" "></textarea>
                    </div>
                    <!--<label for="inputEmail3" class="col-sm-2 col-form-label">Gender</label>
                        <div class="col-sm-10 mt-2" >
                          <label class="radio-inline" for="inputEmail3=0"><input type="radio" name="male" value="male" checked="">Male</label>
                          <label class="radio-inline" for="inputEmail3=1"><input type="radio" name="female" value="female" checked="">Female</label>
                        </div>-->
                  <label for="inputEmail3" class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10 mt-2" >
                          <textarea type="text"  name="edescription" class="form-control"  placeholder=""></textarea>
                    </div>
                   <!--<label for="inputEmail3" class="col-sm-2 col-form-label">Status</label>
                          <div class="col-sm-10 mt-3">
                            <select class="form-control" name="estatus">
                              <option>Active</option>
                            </select>
                          </div>-->
                        <label for="inputEmail3" class="col-sm-2 col-form-label mt-5"></label>

                          <div class="col-sm-6 mt-5">
                         <button class="btn  btn-sm active com text-white" type="submit" >submit</button>
                           <a href="#" class="btn btn-danger btn-sm active">Close</a>
                         </div >
                    </div>


                      </form>

                 @endsection



