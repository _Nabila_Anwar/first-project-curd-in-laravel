@extends('layouts.app')

@section('content')
    <div class="text-right mb-3">
        <a href="{{url('employee')}}" class="btn btn-sm active com text-white">Add</a>
    </div>
<div >
    <label class="row justify-content-center"><h4><strong>Employee</strong></h4></label>
  </div>
    <table class="table table-bordered table-sm text-center ">
      <thead class="text-white">
        <tr  class="com">
          <th class="">SL No.</th>
          <th scope="col">Bramch Name</th>
          <th scope="col">Employee ID</th>
          <th scope="col">Employee Name</th>
          <th scope="col">Father Name</th>
          <th scope="col">Designation</th>
          <th scope="col">Gender</th>
          <th scope="col">Phone</th>
          <th scope="col">Email</th>
          <th scope="col">National ID</th>
          <th scope="col">Present Address</th>
          <th scope="col">Status</th>
          <th scope="col">Action</th>
          </tr>
      </thead>
      <tbody>

        <tr>
          <td></td>
          <td scope="row"></td>
          <td></td>
          <td class="text-center"><a href=""><i class="fa fa-trash" aria-hidden="true"></i></a>
          <a href=""> <i class="fa fa-edit"></i></a></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>
@endsection
