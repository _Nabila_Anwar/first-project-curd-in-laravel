@extends('layouts.app')

@section('content')
<div class="text-right mb-3">
    <a href="{{url('/imagetable')}}" class="btn  btn-sm active com text-white">Company List</a>
</div>
  <div class=" btn btn-block btn-sm active mb-3 com text-white" >New Company</div>

  <form method="post"  action="{{url('/image')}}" enctype="multipart/form-data">
    @csrf
      <div class="form-group row col-md-8 offset-md-2">
            <label for="inputEmail3" class="col-sm-2 col-form-label">First Name*</label>
              <div class="col-sm-10 mt-2" >
                <input type="text"  name="fname" class="form-control"  placeholder="Enter First name">
              </div>
                <label for="inputEmail3" class="col-sm-2 col-form-label">Email*</label>
                    <div class="col-sm-10 mt-2" >
                      <input type="Email"  name="email" class="form-control"  placeholder="Enter email">
                </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Post</label>
                        <div class="col-sm-10 mt-2" >
                          <input type="text"  name="post" class="form-control"  placeholder="Enter post ">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label"> Upload Image</label>
                      <div class="col-sm-10 mt-3" >
                        <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                      </div>

                        <label for="inputEmail3" class="col-sm-2 col-form-label mt-5"></label>

                          <div class="col-sm-6 mt-5">
                         <button class="btn  btn-sm active com text-white" type="submit">submit</button>
                           <a href="#" class="btn btn-danger btn-sm active">Close</a>
                         </div >
                         <div class="col-sm-4 text-right mt-5">
                        <a class="  btn  btn-sm active com text-white" href="{{url('new')}}" >Next</a>
                      </div>

                    </div>


                      </form>

                 @endsection

