@extends('layouts.app')

@section('content')
<div class="text-right mb-3">
    <a href="{{url('student_list')}}" class="btn  btn-sm active com text-white">Student List</a>
</div>
  <div class=" btn btn-block btn-sm active mb-3 com text-white" >Student Information</div>

  <form method="post"  action="{{url('student_update'.$student->id)}}" enctype="multipart/form-data">
    @csrf
      <div class="form-group row col-md-8 offset-md-2">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Student Name*</label>
              <div class="col-sm-10 mt-2" >
                <input type="text"  name="name" class="form-control"  value="{{$student->name}}">
              </div>
                <label for="inputEmail3" class="col-sm-2 col-form-label">Semester*</label>
                    <div class="col-sm-10 mt-2" >
                      <input type="text"  name="semester" class="form-control"  value="{{$student->semester}}">
                </div>
                 <label for="inputEmail3" class="col-sm-2 col-form-label">Roll*</label>
                    <div class="col-sm-10 mt-2" >
                      <input type="number"  name="roll" class="form-control"  value="{{$student->roll}}">
                </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Email*</label>
                        <div class="col-sm-10 mt-2" >
                          <input type="text"  name="email" class="form-control"  value="{{$student->email}}">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10 mt-2" >
                      <textarea type="text"  name="address" class="form-control" value="{{$student->address}}"></textarea>
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">CGPA</label>
                    <div class="col-sm-10 mt-2">
                      <input type="number"  name="cgpa" class="form-control"  value="{{$student->cgpa}}">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Image</label>
                      <div class="col-sm-10 mt-3" >
                        <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                      </div>

                        <label for="inputEmail3" class="col-sm-2 col-form-label mt-5"></label>

                          <div class="col-sm-6 mt-5">
                         <button class="btn  btn-sm active com text-white" type="submit" >submit</button>
                           <a href="#" class="btn btn-danger btn-sm active">Close</a>
                         </div >
                         <div class="col-sm-4 text-right mt-5">
                        <a class="  btn  btn-sm active com text-white" href="{{url('new')}}" >Next</a>
                      </div>

                    </div>


                      </form>

                 @endsection
