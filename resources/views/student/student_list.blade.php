@extends('layouts.app')

@section('content')
    <div class="text-right mb-3">
        <a href="{{url('student_form')}}" class="btn btn-sm active com text-white">Add</a>
    </div>
<div >
    <label class="row justify-content-center"><h4><strong>Students Information</strong></h4></label>
  </div>
    <table class="table table-bordered table-sm text-center ">
      <thead class="text-white">
        <tr  class="com">
          <th class="">Name</th>
          <th scope="col">Semester</th>
          <th scope="col">Roll</th>
          <th scope="col">Email</th>
          <th scope="col">Address</th>
          <th scope="col">CGAP</th>
          <th scope="col">Image</th>
          <th scope="col">Actiion</th>
          </tr>
      </thead>
      <tbody>
        @foreach($student as $std)
        <tr>
          <td>{{$std->name}}</td>
          <td scope="row">{{$std->semester}}</td>
          <td>{{$std->roll}}</td>
          <td class="text-center">{{$std->email}}</td>
          <td>{{$std->address}}</td>
          <td>{{$std->cgpa}}</td>
          <td><img src="{{asset('uploads/imagefolder/'. $std->image)}}" width="100px;" height="100px;" alt="Image"></td>
          <td><a href="{{url('student_update'.$std->id)}}" class="btn btn-warning">Update</a>
            <a href="{{url('delete/student'.$std->id)}}" class="btn btn-warning">DELETE</a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
@endsection
