<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/form', function(){
	return view('test');
});
Route::get('/union_zone','UnionController@index');
Route::post('/union_zone','UnionController@store');
Route::get('/unionlist', 'UnionController@show');
Route::get('/unionedit{id}', 'UnionController@edit');
Route::get('/village_area','VillageController@index');
Route::post('/village_area','VillageController@store');
Route::get('/villagelist', 'VillageController@show');
Route::get('/samity', 'SamityController@index');
Route::post('/samity', 'SamityController@store');
Route::get('/samitylist','SamityController@show' );
Route::get('/samityedit{id}','SamityController@edit' );
Route::post('/samityedit{id}','SamityController@update' );
Route::get('delete/samity{id}','SamityController@destroy' );
Route::get('/employee', function(){
	return view('employee.employee');
});
Route::get('/employee_list', 'EmployeesController@index');
Route::post('/employee','EmployeesController@store');
Route::get('/image', 'ImageController@index');
Route::post('/image', 'ImageController@store');
Route::get('/imagetable', 'ImageController@show');

Route::get('/student_form','StudentController@index');
Route::post('/student_form','StudentController@store');
Route::get('/student_list','StudentController@show');
Route::get('/student_update{id}','StudentController@edit');
Route::post('/student_update{id}','StudentController@update');
Route::get('/delete/student{id}','StudentController@destroy');