<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Samity extends Model
{
	use SoftDeletes;
    protected $table= 'samities';
    protected $fillable = ['sbranch_name','samity'];
}
