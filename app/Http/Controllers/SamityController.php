<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Samity;

class SamityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('address.samity');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $samity = new Samity;
        $samity->sbranch_name = $request->sbranch_name;
        $samity->samity= $request->samity;
        $samity->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $samities = Samity::all();
        return view('address.samitylist')->with('samities',$samities);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //using model
        $samity = Samity::findorfail($id);
        return view('address.samityedit',compact('samity'));
        //using DB
        /*$samity=DB::table('samities')->where('id',$id)->first();
        return view('address.samityedit', compact('samity'));*/

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //usong model
        $samity = Samity::findorfail($id);
        $samity->sbranch_name = $request->sbranch_name;
        $samity->samity= $request->samity;
        $samity->save();
        //using DB
        /*$new_sbranch_name = $request->input('sbranch_name');
        $new_samity = $request->input('samity');

        DB::update('update samities set sbranch_name = ?, samity = ? where id = ?',[$new_sbranch_name,$new_samity,$id]);*/
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $samity = Samity::findorfail($id);
        $samity->delete();
        return back();
        //DB
        //DB::table('samities')->where('id',$id)->delete();
    }
}
