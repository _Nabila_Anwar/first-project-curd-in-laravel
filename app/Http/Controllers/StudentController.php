<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use Illuminate\Support\Str;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('student.student_form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student = new Student;
        $student->name = $request->name;
        $student->roll = $request->roll;
        $student->semester = $request->semester;
        $student->email = $request->email;
        $student->address = $request->address;
        $student->cgpa = $request->cgpa;
        if($request->hasfile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename=Str::slug($student->roll).'.'.$extension;
            $file->move('uploads/imagefolder/',$filename);
            $student->image = $filename;
        }
        else{
            return $request;
            $student->image = '';
        }
        $student->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $student = Student::all();
        return view('student.student_list')->with('student',$student);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::findorfail($id);
        return view('student.student_update',compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Student::findorfail($id);
        $student->name = $request->name;
        $student->roll = $request->roll;
        $student->semester = $request->semester;
        $student->email = $request->email;
        $student->address = $request->address;
        $student->cgpa = $request->cgpa;
        if($request->hasfile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename=Str::slug($student->roll).'.'.$extension;
            $file->move('uploads/imagefolder/',$filename);
            $student->image = $filename;
        }
        else{
            return $request;
            $student->image = '';
        }
        $student->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::findorfail($id);
        $student->delete();
        return back();
    }
}
